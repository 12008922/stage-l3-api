from django.urls import path

from .views import GPXView

urlpatterns = [
    path('api/', GPXView.as_view(), name='gpx-view'),
]
