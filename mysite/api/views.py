from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from django.http import HttpResponse
import requests
import networkx as nx
import xml.etree.ElementTree as ET
import io
import math
import time
import concurrent.futures

# recherche le noeud le plus proche de lat lon
def nearest_node(lat, lon, G):
    min_dist = float('inf')
    closest_node = None
    for node in G.nodes(data=True):
        node_lat = node[1]['lat']
        node_lon = node[1]['lon']
        dist = (lat - node_lat) ** 2 + (lon - node_lon) ** 2
        if dist < min_dist:
            min_dist = dist
            closest_node = node[0]
    return closest_node

# calcule la distance entre 2 points
def distance(lat1, lon1, lat2, lon2):
    R = 6371000  # Rayon de la Terre en mètres
    phi1 = math.radians(lat1)
    phi2 = math.radians(lat2)
    delta_phi = math.radians(lat2 - lat1)
    delta_lambda = math.radians(lon2 - lon1)

    a = math.sin(delta_phi / 2.0) ** 2 + math.cos(phi1) * math.cos(phi2) * math.sin(delta_lambda / 2.0) ** 2
    c = 2 * math.atan2(math.sqrt(a), math.sqrt(1 - a))

    return R * c

# genere des points intermediaire à intervalle de 20km
def intermediate_points(lat1, lon1, lat2, lon2, interval=20000):
    dist = distance(lat1, lon1, lat2, lon2)
    if dist <= interval:
        return [(lat1, lon1), (lat2, lon2)]
    
    points = [(lat1, lon1)]
    steps = int(dist // interval)
    for i in range(1, steps + 1):
        f = i / (steps + 1)
        lat = lat1 + f * (lat2 - lat1)
        lon = lon1 + f * (lon2 - lon1)
        points.append((lat, lon))
    points.append((lat2, lon2))
    return points

# fonction qui execute la requete overpass
def fetch_overpass_data(lat1, lon1, lat2, lon2, distance_m, types):
    lat = (lat1 + lat2)/2
    lon = (lon1 + lon2)/2
    distance = distance_m + 2000
    overpass_url = "http://overpass-api.de/api/interpreter"
    overpass_query = ""
    match types:
        case "0": 
            overpass_query = f"""
                [out:json][timeout:300][maxsize:1073741824];
                (
                    way["highway"~"^(primary|motorway|trunk|secondary|tertiary|unclassified|residential|motorway_link|primary_link|trunk_link|secondary_link|tertiary_link)$"]
                    (around:{distance},{lat},{lon});
                );
                (._;>;);
                out body;
                """
        case "1":
            overpass_query = f"""
                [out:json][timeout:300][maxsize:1073741824];
                (
                    way["highway"~"^(footway|bridleway|steps|corridor|path|via_ferrata|crossing|residential|unclassified|secondary|tertiary)$"]
                    (around:{distance},{lat},{lon});
                );
                (._;>;);
                out body;
                """
        case "2":
            overpass_query = f"""
                [out:json][timeout:300][maxsize:1073741824];
                (
                    way["highway"~"^(cycleway|primary|secondary|tertiary|unclassified|residential|primary_link|secondary_link|tertiary_link)$"](around:{distance},{lat},{lon});
                );
                (._;>;);
                out body;
                """
        case "3":
            overpass_query = f"""
                [out:json][timeout:300][maxsize:1073741824];
                (
                    way["highway"~"^(primary|secondary|tertiary|unclassified|residential|primary_link|secondary_link|tertiary_link)$"](around:{distance},{lat},{lon});
                );
                (._;>;);
                out body;
                """
        case _:
            overpass_query = """
                [out:json];
                (._;>;);
                out body;
                """
    
    response = requests.get(overpass_url, params={'data': overpass_query})
    if response.status_code != 200:
        raise Exception(f"Failed to retrieve data from Overpass API with status code {response.status_code}")
    return response.json()

class GPXView(APIView):
    def get(self, request, *args, **kwargs):
        # recuperation des parametres
        lat1 = request.query_params.get('lat1')
        lon1 = request.query_params.get('lon1')
        lat2 = request.query_params.get('lat2')
        lon2 = request.query_params.get('lon2')
        types = request.query_params.get('type')  # 0: vehicule motorisé, 1: piéton, 2: cycliste 3: vehicule sans permis

        if not all([lat1, lon1, lat2, lon2, types]):
            return Response({"error": "Please provide lat1, lon1, lat2, and lon2 parameters."},
                            status=status.HTTP_400_BAD_REQUEST)

        # string to float
        try:
            lat1, lon1, lat2, lon2 = map(float, [lat1, lon1, lat2, lon2])
        except ValueError:
            return Response({"error": "Invalid latitude or longitude values."},
                            status=status.HTTP_400_BAD_REQUEST)

        # genere les point inter
        points = intermediate_points(lat1, lon1, lat2, lon2)

        overpass_data = []
        
        #thread des requetes overpass
        try:
            with concurrent.futures.ThreadPoolExecutor(max_workers=4) as executor:
                futures = []
                for i in range(len(points) - 1):
                    time.sleep(1)
                    futures.append(executor.submit(fetch_overpass_data, points[i][0], points[i][1], points[i+1][0], points[i+1][1], distance(points[i][0], points[i][1], points[i+1][0], points[i+1][1]), types))
                    
                for future in concurrent.futures.as_completed(futures):
                    try:
                        overpass_data.append(future.result())
                    except requests.exceptions.HTTPError as exc:
                        if exc.response.status_code == 429:  # Too many requests
                            time.sleep(60)
                            futures.append(executor.submit(fetch_overpass_data, points[i][0], points[i][1], points[i+1][0], points[i+1][1], distance(points[i][0], points[i][1], points[i+1][0], points[i+1][1]), types))
                        else:
                            return Response({"error1": str(exc)}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        except Exception as e:
            return Response({"error2": str(e)}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)



        # Combiner les résultats des appel api
        combined_data = {'elements': []}
        for data in overpass_data:
            combined_data['elements'].extend(data['elements'])

        
        def heuristic1(node1, node2):
            lat1 = G.nodes[node1]['lat']
            lon1 = G.nodes[node1]['lon']
            lat2 = G.nodes[node2]['lat']
            lon2 = G.nodes[node2]['lon']
            return distance(lat1, lon1, lat2, lon2)
        
        def time_travel(nodes1,nodes2,speed):
            return heuristic1(nodes1,nodes2) / speed

        # Construire le graphe avec networkx
        G = nx.Graph()

        for element in combined_data['elements']:
            if element['type'] == 'way':
                match types:
                    case "0":
                        if element.get('tags', {}).get('maxspeed') is None : 
                            match element.get('tags',{}).get('highway'):
                                case "motoway":
                                    maxspeed = 120.0 / 3.6
                                case "trunk":
                                    maxspeed = 120.0/ 3.6
                                case "motorway_link":
                                    maxspeed = 70.0 / 3.6
                                case "trunk_link":
                                    maxspeed = 70 / 3.6
                                case _:
                                    maxspeed = 40 / 3.6
                        elif element.get('tags', {}).get('maxspeed') == "signals":
                            match element.get('tags',{}).get('highway'):
                                case "motoway":
                                    maxspeed = 120.0 / 3.6
                                case "trunk":
                                    maxspeed = 120.0/ 3.6
                                case "motorway_link":
                                    maxspeed = 70.0 / 3.6
                                case "trunk_link":
                                    maxspeed = 70 / 3.6
                                case _:
                                    maxspeed = 50 / 3.6
                        elif element.get('tags', {}).get('maxspeed') == 'walk':
                            maxspeed = 5.0 / 3.6
                        else:
                            maxspeed = float(element.get('tags', {}).get('maxspeed')) /3.6
                    case "1":
                        maxspeed = 5.0 / 3.6
                    case "2":
                        maxspeed = 20.0 / 3.6
                    case "3":
                        if element.get('tags', {}).get('maxspeed') is None : 
                            maxspeed = 45 / 3.6
                        elif element.get('tags', {}).get('maxspeed') == "signals":
                            maxspeed = 45 / 3.6
                        elif element.get('tags', {}).get('maxspeed') == 'walk':
                            maxspeed = 5.0 / 3.6
                        else:
                            if float(element.get('tags', {}).get('maxspeed')) > 45:
                                maxspeed = 45 / 3.6
                            else:
                                maxspeed = float(element.get('tags', {}).get('maxspeed')) /3.6

                nodes = element['nodes']
                for i in range(len(nodes) - 1):
                    G.add_edge(nodes[i], nodes[i + 1],maxspeed=maxspeed)
                    
            elif element['type'] == 'node':
                tags = element.get('tags', {})
                G.add_node(element['id'], **tags, lat=element['lat'], lon=element['lon'])

        for u, v, data in G.edges(data=True):
            G[u][v]['speed_average'] = time_travel(u,v,data.get('maxspeed'))

        orig_node = nearest_node(lat1, lon1, G)
        dest_node = nearest_node(lat2, lon2, G)
        print(orig_node)
        print(dest_node)
        print(G)

        try:
            shortest_path = nx.astar_path(G, orig_node, dest_node, heuristic=None, weight="speed_average")
        except nx.NetworkXNoPath:
           return Response({"error": "No path found"}, status=status.HTTP_404_NOT_FOUND)

        # Créer le fichier GPX
        gpx = ET.Element("gpx", version="1.1", creator="Django GPX API")
        trk = ET.SubElement(gpx, "trk")
        trkseg = ET.SubElement(trk, "trkseg")

        for node in shortest_path:
            point = G.nodes[node]
            trkpt = ET.SubElement(trkseg, "trkpt", lat=str(point['lat']), lon=str(point['lon']))
            ET.SubElement(trkpt, "ele").text = "Point"

        gpx_data = ET.tostring(gpx, encoding="utf-8", method="xml")

        gpx_file = io.BytesIO(gpx_data)

        response = HttpResponse(gpx_file, content_type='application/gpx+xml')
        response['Content-Disposition'] = 'attachment; filename="route.gpx"'

        return response
