 # Guide d'Installation et d'Utilisation de l'API Django

## Installation de Django et des paquets : 
```
pip install django djangorestframework requests networkx 
```

## Récupérer les fichiers : 
```
git clone https://gitlab.sorbonne-paris-nord.fr/12008922/stage-l3-api
```
## Une fois ces deux étapes finies mettez-vous dans le repertoire "stage-l3-api/mysite" : 
```
cd stage-l3-api/mysite
```

## Lancez le serveur : 
```
python3 manage.py runserver
```
## Voici un exemple de lien que vous pouvez ensuite mettre sur votre navigateur : 
http://127.0.0.1:8000/api/?lat1=48.968898&lon1=2.600642&lat2=48.937169&lon2=2.409229&type=0
 - lat1 et lon1 représentent la latitude et longitude du premier point et lat2 et lon2 représentent la latitude et longitude du deuxieme point
 - Ici le type représente le moyen de transport utilisé : (0: vehicule motorisé, 1: piéton, 2: cycliste 3: vehicule sans permis)

## Un fichier gpx est alors téléchargé.

Si vous souhaitez effectuer un trajet ayant des points intermediaires, il faut alors changer de branch en faisant :
```
git switch points_inter 
```
Puis suivre le Read.me car la version points_inter étant pas stable, nous avons preferé ne pas fusionner les 2 versions tant qu'elle n'est pas totalement stable.
